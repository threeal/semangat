#include <iostream>
#include <cmath>

int main(int argc, char **argv)
{
  double angle;

  std::cin >> angle;

  if (angle == 180.0 || angle == -180.0)
  {
    std::cout << angle << std::endl;
    return 0;
  }

  double dividen = (angle + 180.0) / 360.0;
  double modulo = angle - (floor(dividen) * 360.0);

  std::cout << modulo << std::endl;

  return 0;
}