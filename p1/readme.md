# Angle

## Description

Inputs is an angle value in degree. Output normalized value of the angle such that it stays between `-180.0 < value < 180.0`.

## Example

Input
```
270
```

Output
```
-90
```