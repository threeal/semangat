#include <iostream>
#include <string>
#include <vector>

bool findSquare(
  int x, int y, const int &width, const int &height,
  const std::vector<std::string> &canvas)
{
  int xx = x + 1;
  for (; xx < width; ++xx)
  {
    if (canvas[y][xx] != '#')
      break;
  }

  --xx;
  if (xx - x < 1)
    return false;

  int yy = y + 1;
  for (; yy < height; ++yy)
  {
    if (canvas[yy][x] != '#')
      break;
  }

  --yy;
  if (yy - y < 1)
    return false;

  for (int yyy = y - 1; yyy <= yy + 1; ++yyy)
  {
    for (int xxx = x - 1; xxx <= xx + 1; ++xxx)
    {
      if (xxx < 0 || xxx >= width)
        continue;

      if (yyy < 0 || yyy >= height)
        continue;

      if (xxx == x - 1 || xxx == xx + 1 || yyy == y - 1 || yyy == yy + 1)
      {
        if (canvas[yyy][xxx] == '#')
          return false;
      }
      else
      {
        if (canvas[yyy][xxx] != '#')
          return false;
      }
    }
  }

  return true;
}

int main(int argc, char **argv)
{
  int width;
  std::cin >> width;

  int height;
  std::cin >> height;

  std::vector<std::string> canvas;
  canvas.resize(height);

  std::string str;
  for (int y = 0; y < height; ++y)
  {
    std::cin >> str;
    canvas[y] = str;
  }

  for (int y = 0; y < height; ++y)
  {
    for (int x = 0; x < width; ++x)
    {
      if (canvas[y][x] == '#')
      {
        if (findSquare(x, y, width, height, canvas))
        {
          std::cout << "yes" << std::endl;
          return 0;
        }
      }
    }
  }

  std::cout << "no" << std::endl;

  return 0;
}