# Rectangle

## Description

Inputs are the width and the height of canvas followed by patterns using `_` and `#` characters. Output `yes` if there is at least one rectangle pattern using `#` with a minimum size of 2 x 2. else, output `no`.

## Example

Input
```
6 3
_###__
_###__
______
```

Output
```
yes
```