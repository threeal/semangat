# Words

## Description

Input N number of words, output words that have been inputted more than once, sorted from A-Z.

## Example
Input:
```
7
zeus
phoenix
zeus
ares
medusa
ares
ares
```
Output:
```
ares
zeus
```