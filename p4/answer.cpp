#include <iostream>
#include <map>
#include <string>

int main(int argc, char **argv)
{
  std::map<std::string, int> word_map({ });

  int n;
  std::cin >> n;

  std::string str;
  for (int i = 0; i < n; i++)
  {
    std::cin >> str;
    auto word = word_map.find(str);
    if (word != word_map.end())
    {
      word->second++;
    }
    else
    {
      word_map.emplace(str, 1);
    }
  }

  for (auto word : word_map)
  {
    if (word.second > 1)
      std::cout << word.first << " ";
  }
  std::cout << std::endl;

  return 0;
}