#include <iostream>

int main(int argc, char **argv)
{
  long long sum = 0;

  long long a;
  while (!std::cin.eof())
  {
    std::cin >> a;
    sum += a;
  }

  std::cout << sum << std::endl;

  return 0;
}