# Sum

## Description

Given some integers as inputs, output the sum of all the inputted integers.

## Example

Input:
```
2
4
10
13
```

Output:
```
29
```